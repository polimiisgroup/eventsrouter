var mqtt = require('mqtt');
var fs = require('fs');
var xml2js = require('xml2js');
var Client = require('node-rest-client').Client;
var client = new Client();
var express = require('express');
var app = express();
app.use(express.static(__dirname));

var USER = 'admin';
var PASS = 'password';
var clientId = 'ntstmqtt_' + Math.random().toString(16).substr(2, 8);
var BrokerHost = 'localhost';
var BrokerPort = 1883;
var uuid = require('node-uuid');
var remoteArtifacts = [];

var mappingPath = 'mapping.xml';


if(process.argv.length > 4) {
	BrokerHost = process.argv[2];
	USER = process.argv[3];
	PASS = process.argv[4];
}

if(process.argv.length > 5) {
    mappingPath = process.argv[5];
}

var opts = { host: BrokerHost, port: BrokerPort, username: USER, password : PASS, keepalive: 30, clientId: clientId };
var mqttclient = mqtt.connect(opts);




var publisher;
var subscriber = {};
var externalcep = false;
var artifactId;
var artifactName;

client.registerMethod("sendDataToLocalCEP", "http://localhost:1880/api/data", "GET");

client.registerMethod("notifyPASO", "http://localhost:8083/api/updateInfoModel", "GET");

client.registerMethod("getInfoModel", "http://localhost:8083/api/infoModel", "GET");

app.get('/api/rereadbinding', function (req, res) {
   var parseString = xml2js.parseString;
    parseString(fs.readFileSync(mappingPath, 'utf8'), function (err, result) {
      //TODO, aggiungere la gestione dell'errore
      console.log('parsed');
	  initConnections(result);
    });
   res.end('Binding file reread');
})

app.get('/api/simulateevent', function (req, res) {
	if (req.param('eventid')!=undefined){
		//find if the event is responsible for communicating a change in an artifact instance
          for (var artifact in remoteArtifacts){
              for (var br in remoteArtifacts[artifact].bindingEvents){
                  if(req.param('eventid') == remoteArtifacts[artifact].bindingEvents[br]) {
                      //event body has a field indicating the new artifact instance
                      if(req.param('data')!=undefined) {
                        if(remoteArtifacts[artifact].id != req.param('data')){
                            //a previous artifact instance has been defined
                            if(remoteArtifacts[artifact].id !=''){
                                unsubscribe(remoteArtifacts[artifact].name+'/'+remoteArtifacts[artifact].id+'/status');
                            }
                            remoteArtifacts[artifact].id = req.param('data');
                            //the field in the event body is not empty
                            if (remoteArtifacts[artifact].id != '') {
                                createSubscriber(remoteArtifacts[artifact].name+'/'+remoteArtifacts[artifact].id+'/status');
                            }
                        }
                      }
                  }

              }

			  for (var ur in remoteArtifacts[artifact].unbindingEvents){
                  if(req.param('eventid') == remoteArtifacts[artifact].unbindingEvents[ur]) {
                      unsubscribe(remoteArtifacts[artifact].name+'/'+remoteArtifacts[artifact].id+'/status');
                      remoteArtifacts[artifact].id = '';
                  }
              }
			  }

          client.methods.notifyPASO({parameters: {name: (req.param('eventid')), value: ''}}, function (data, response) {

          });
	}
	res.end('Event sent');
})

//TODO: da cambiare in PUT, fornendo le letture come oggetto JSON
app.get('/api/data', function (req, res) {
   var obj = {}
   obj['id'] = 'foobar';
   obj['timestamp'] = Date.now();

   for(var key in req.query) {
       obj[key] = req.param(key);
   }
    if(externalcep) {
       objPayload.payloadData = obj;
       objEvent.event = objPayload;
        mqttclient.publish(artifactName+'/'+artifactId+'/data', JSON.stringify(objEvent));
        res.end(JSON.stringify(objEvent));
   } else {
       var args = {};
       args['parameters'] = obj;
       console.log(JSON.stringify(args));
        client.methods.sendDataToLocalCEP(args, function (data, response) {

        });
       res.end('CEPInvoked');
   }
})

app.get('/api/status', function (req, res) {
   if(!externalcep) {
        //publish status on MQTT for other artifacts
        var objContainer = {};
        objContainer.id = 'foobar';
        objContainer.status = req.param('status');
        objContainer.timestamp = Date.now();
        objPayload.payloadData = objContainer;
        objEvent.event = objPayload;
        mqttclient.publish(artifactName+'/'+artifactId+'/status', JSON.stringify(objEvent));
		//send status to PASO
        client.methods.notifyPASO({parameters: {name: artifactName, value: req.param('status')}}, function (data, response) {
        		 // parsed response body as js object
	           console.log(data);
	           // raw response
	           console.log(response);
              });

        res.end(JSON.stringify(objEvent));
   } else {
        res.end('NotActive');
   }
})


var server = app.listen(8081, function () {
  var port = server.address().port;
  console.log("Broker listening on port "+port);
})


function createPublisher(topcMQTT) {
	console.log('publishing on topic '+topcMQTT);
}

function createSubscriber(topcMQTT) {
	console.log('subscribed to topic '+topcMQTT);
	mqttclient.subscribe(topcMQTT);
}

function unsubscribe(topcMQTT) {
	console.log('unsubscribed to topic '+topcMQTT);
	mqttclient.unsubscribe(topcMQTT);
}

function initConnections(mapping) {
	console.log('startmap');
	for (var la in mapping['martifact:definitions']['martifact:localArtifact']){
		artifactName=mapping['martifact:definitions']['martifact:localArtifact'][la]['$'].name;
		artifactId=mapping['martifact:definitions']['martifact:localArtifact'][la]['$'].id;
		externalcep=mapping['martifact:definitions']['martifact:localArtifact'][la]['$'].externalCep;
	}
	if (artifactId!=undefined && artifactName!=undefined){
		if(externalcep) {
			createPublisher(artifactName+'/'+artifactId+'/data');
			createSubscriber(artifactName+'/'+artifactId+'/status');
		} else {
			createPublisher(artifactName+'/'+artifactId+'/status');
		}
	}
	var ra = mapping['martifact:definitions']['martifact:mapping'][0]['martifact:artifact'];
    remoteArtifacts = [];
	for(var artifact in ra) {
      var br = [];
      for (var aid in ra[artifact]['martifact:bindingEvent']){
          br.push(ra[artifact]['martifact:bindingEvent'][aid]['$'].id);
      }
	  var ur = [];
      for (var aid in ra[artifact]['martifact:unbindingEvent']){
          ur.push(ra[artifact]['martifact:unbindingEvent'][aid]['$'].id);
      }
      remoteArtifacts.push({name: ra[artifact]['$'].name, id: '', bindingEvents: br, unbindingEvents: ur});
	}
    var stakeHolders = mapping['martifact:definitions']['martifact:stakeholder'];
    for(var key in stakeHolders) {
      createSubscriber(stakeHolders[key]['$'].name+'/'+stakeHolders[key]['$'].processInstance);
	}
}


mqttclient.on('connect', function () {
  console.log('MQTT client connected');




});

mqttclient.on('reconnect', function () {
	console.log('MQTT client reconnected');
});


mqttclient.on('error', function (error) {
  console.log('MQTT client error' + error);
});

mqttclient.on('message', function (topic, message) {
  console.log([topic, message].join(": "));
  var elements = topic.split('/')
      console.log(elements.length)
      if(elements.length==3) {
          if(elements[2]=='status'){

              client.methods.notifyPASO({parameters: {name: elements[0], value: (JSON.parse(message.toString())).event.payloadData.status, timestamp: (JSON.parse(message.toString())).event.payloadData.timestamp}}, function (data, response) {

              });

          }
      } else if (elements.length==2) {

          //find if the event is responsible for communicating a change in an artifact instance
          for (var artifact in remoteArtifacts){
              for (var br in remoteArtifacts[artifact].bindingEvents){
                  if(JSON.parse(message.toString()).event.payloadData.eventid == remoteArtifacts[artifact].bindingEvents[br]) {
                      //event body has a field indicating the new artifact instance
                      if('data' in (JSON.parse(message.toString())).event.payloadData) {
                        if(remoteArtifacts[artifact].id != JSON.parse(message.toString()).event.payloadData.data){
                            //a previous artifact instance has been defined
                            if(remoteArtifacts[artifact].id !=''){
                                unsubscribe(remoteArtifacts[artifact].name+'/'+remoteArtifacts[artifact].id+'/status');
                            }
                            remoteArtifacts[artifact].id = JSON.parse(message.toString()).event.payloadData.data;
                            //the field in the event body is not empty
                            if (remoteArtifacts[artifact].id != '') {
                                createSubscriber(remoteArtifacts[artifact].name+'/'+remoteArtifacts[artifact].id+'/status');
                            }
                        }
                      }
                  }

              }
			  for (var ur in remoteArtifacts[artifact].unbindingEvents){
                  if(JSON.parse(message.toString()).event.payloadData.eventid == remoteArtifacts[artifact].unbindingEvents[ur]) {
                      unsubscribe(remoteArtifacts[artifact].name+'/'+remoteArtifacts[artifact].id+'/status');
                      remoteArtifacts[artifact].id = '';
                  }
              }
          }

          client.methods.notifyPASO({parameters: {name: (JSON.parse(message.toString())).event.payloadData.eventid, value: ''}}, function (data, response) {

          });
      }
});




var objEvent = {};
var objPayload = {};


var random = require("node-random");
var   idCamion = uuid.v4();
var   idContainer = uuid.v4();

