Software component to route artifact-related events to the PASO E-GSM engine. Mechanisms to bind and unbind artifacts to IoT smart objects at runtime are provided.


